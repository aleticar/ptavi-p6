
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys

# Cliente UDP simple.
# Dirección IP del servidor.
# Contenido que vamos a enviar
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto

try:
    method = str(sys.argv[1])
    receiver = str(sys.argv[2])
except ValueError:
    sys.exit(" SIP/2.0 400 Bad Request ")
except IndexError:
    sys.exit(" Usage: python3 client.py method receiver@IP:SIPport ")

receiver1 = receiver.split(":")
receiver2 = receiver1[0].split("@")


if method == "INVITE":
    LINE = (method + " sip:" + receiver[0:20] + " SIP/2.0\r\n")

if method == "BYE":
    LINE = (method + " sip:" + receiver[0:20] + " SIP/2.0\r\n")


with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

    ADDRESS = receiver2[1]
    LOGIN = receiver2[0]
    PORT = receiver1[1]
    SERVER = "127.0.0.1"
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, int(PORT)))

    print("Enviando: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)

    print('Recibido -- ', data.decode('utf-8'))
    if method == "INVITE":
        intento = "SIP/2.0 100 TRYING"
        llamada = "SIP/2.0 180 RING"
        recibo = "SIP/2.0 200 OK"
        transformo = data.decode('utf-8').split("\r\n\r\n")

        if transformo[2] == "SIP/2.0 200 OK":
            method2 = "ACK"
            LINE1 = (method2 + " sip:" + receiver + "\r\n" + " SIP/2.0\r\n")
            print("Enviando:" + LINE1)
            my_socket.send(bytes(LINE1, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024)
            print('Recibido -- ', data.decode('utf-8'))

    print("Terminando socket...")

print("Fin.")
