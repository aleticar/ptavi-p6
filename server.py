"""Echo server class."""

import socketserver
import simplertp
import secrets
import sys


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""
    def handle(self):
        """Echo server class."""
        # Escribe dirección y puerto del cliente (de tupla client_address)
        self.wfile.write(b"Hemos recibido tu peticion")
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            print("El cliente nos manda " + line.decode('utf-8'))

            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break
            camb = line.decode('utf-8')
            cambio = camb.split(" ")
            print(cambio)
            if cambio != "\r\n":
                receiver1 = cambio[1].split("@")[1]
                username = cambio[1].split("@")[0].split(":")[1]
                print(username)
                port = cambio[1].split("@")[1].split(":")[0]
                print(port)
                v = "v = 0\r\n"
                o = 'O = ' + str(username) + ' ' + str(receiver1) + '\r\n'
                s = 's = mysession\r\n'
                t = 't = 0\r\n'
                m = 'm = audio ' + str(port) + ' RTP\r\n'
                print("El cliente nos manda " + line.decode('utf-8'))
                todo = v + s + t + o + m

            if cambio == 'sip':
                self.wfile.write(b" SIP/2.0 400 Bad Request\r\n\r\n")

            if cambio[0] == "INVITE":

                escribe = "\r\nSIP/2.0 100 TRYING\r\n\r\n"
                escribe1 = escribe + "SIP/2.0 180 RING\r\n\r\n"
                escribe2 = escribe1 + "SIP/2.0 200 OK\r\n\r\n"
                lent = len(todo)
                len1 = ('Content-Lenght = ' + str(lent))
                escribe3 = escribe2 + len1 + str(todo)

                self.wfile.write(bytes(escribe3, "utf-8"))
            elif cambio[0] == "BYE":
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            elif cambio[0] == 'ACK':
                BIT = secrets.randbelow(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT, payload_type=14,
                                      ssrc=200002)
                audio = simplertp.RtpPayloadMp3(ARCHIVE)
                ip = '127.0.0.1'
                puerto = 23032
                simplertp.send_rtp_packet(RTP_header, audio, ip, puerto)
            elif cambio != ("INVITE", "BYE", "ACK"):
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":
    try:
        IP = sys.argv[1]
        PORT = sys.argv[2]
        ARCHIVE = sys.argv[3]

    except IndexError:
        sys.exit("Usage: python3 server.py IP port audio_file")

    serv = socketserver.UDPServer((IP, int(PORT)), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()
